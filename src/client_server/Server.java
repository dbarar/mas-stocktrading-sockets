package client_server;
// Java implementation of  Server side
// It contains two classes : Server and ClientHandler
// Save file as Server.java

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;


public class Server {
    static Maps maps = new Maps();

    public static void setMaps(Maps maps){
        Server.maps = maps;
    }

    public static void main(String[] args) throws IOException {
        // server is listening on port 5056
        ServerSocket ss = new ServerSocket(5056);

        // running infinite loop for getting
        // client request
        while (true) {
            Socket s = null;
            try {
                // socket object to receive incoming client requests
                s = ss.accept();

                System.out.println("A new client is connected : " + s);

                // obtaining input and out streams
                DataInputStream dis = new DataInputStream(s.getInputStream());
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());

                System.out.println("Assigning new thread for this client");

                // create a new thread object
                Thread t = new ClientHandler(maps, s, dis, dos);

                // Invoking the start() method
                t.start();
            } catch (Exception e) {
                assert s != null;
                s.close();
                e.printStackTrace();
            }
        }
    }

    public static class Maps {
        Map<Integer, Integer> buyMap = new HashMap<>();
        Map<Integer, Integer> sellMap = new HashMap<>();
        Map<Integer, Integer> holdMap = new HashMap<>();
    }
}

class ClientHandler extends Thread {
    final DataInputStream dis;
    final DataOutputStream dos;
    final Socket s;
    Server.Maps maps;

    public ClientHandler(Server.Maps maps,
                         Socket s,
                         DataInputStream dis,
                         DataOutputStream dos) {
        this.maps = maps;
        this.s = s;
        this.dis = dis;
        this.dos = dos;
    }

    @Override
    public void run() {
        String received;
        while (true) {
            try {
                received = dis.readUTF();
                String[] receivedList = received.split(" ");
                int id = Integer.parseInt(receivedList[0]);
                String action = receivedList[1];

                System.out.println("in Client Handler received: " + id + action);

                if (received.equals("Exit")) {
                    System.out.println("Client " + this.s + " sends exit...");
                    System.out.println("Closing this connection.");
                    this.s.close();
                    System.out.println("Connection closed");
                    break;
                }
                switch (action) {
                    case "UPDATE": // for StockAgent Client
                        if (maps.buyMap.containsKey(id) && maps.buyMap.get(id) > 0) {
                            dos.writeUTF(maps.buyMap.get(id) + " BUY");
                            maps.buyMap.put(id, 0);
                        }
                        if (maps.sellMap.containsKey(id) && maps.sellMap.get(id) > 0) {
                            dos.writeUTF(maps.sellMap.get(id) + " SELL");
                            maps.sellMap.put(id, 0);
                        }
                        if (maps.holdMap.containsKey(id) && maps.holdMap.get(id) > 0) {
                            dos.writeUTF(maps.holdMap.get(id) + " HOLD");
                            maps.holdMap.put(id, 0);
                        }
                        dos.writeUTF(0 + " BEAT");
                        break;
                    case "BUY": // for InvestorAgent Client
                        if (maps.buyMap.containsKey(id)) {
                            maps.buyMap.put(id, maps.buyMap.get(id) + 1);
                        } else {
                            maps.buyMap.put(id, 1);
                        }
                        break;
                    case "SELL": // for InvestorAgent Client
                        if (maps.sellMap.containsKey(id)) {
                            maps.sellMap.put(id, maps.sellMap.get(id) + 1);
                        } else {
                            maps.sellMap.put(id, 1);
                        }
                        break;
                    case "HOLD": // for InvestorAgent Client
                        if (maps.holdMap.containsKey(id)) {
                            maps.holdMap.put(id, maps.holdMap.get(id) + 1);
                        } else {
                            maps.holdMap.put(id, 1);
                        }
                        break;
                    default:
                        dos.writeUTF("Invalid input");
                        break;
                }
                Server.setMaps(maps);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            // closing resources
            this.dis.close();
            this.dos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}