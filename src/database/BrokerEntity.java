package database;

public class BrokerEntity {
    private int id;

    private String name;
    private int confidence;
    private int stocks;
    private int price;

    public BrokerEntity(int id, String name, int confidence, int stocks, int price) {
        this.id = id;
        this.name = name;
        this.confidence = confidence;
        this.stocks = stocks;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }

    public int getStocks() {
        return stocks;
    }

    public void setStocks(int stocks) {
        this.stocks = stocks;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BrokerEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", confidence=" + confidence +
                ", stocks=" + stocks +
                ", price=" + price +
                '}';
    }


}
