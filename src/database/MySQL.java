package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQL {
    private static String connectionUrl = "jdbc:mysql://localhost:3306/agents?serverTimezone=UTC";

    public static void main(String[] args) {
        MySQL mySQL = new MySQL();
        mySQL.selectAllBrokers();
        mySQL.updateBroker(new BrokerEntity(1, "ION", 10, 3, 3));
        mySQL.selectAllBrokers();
    }

    public List<BrokerEntity> selectAllBrokers() {
        String sqlSelectAllBrokers = "SELECT * FROM broker";
        try (Connection conn = DriverManager.getConnection(connectionUrl, "root", "root");
             PreparedStatement ps = conn.prepareStatement(sqlSelectAllBrokers);
             ResultSet rs = ps.executeQuery()) {
            List<BrokerEntity> brokerEntityList = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int confidence = rs.getInt("confidence");
                int stocks = rs.getInt("stocks");
                int price = rs.getInt("price");

                BrokerEntity brokerEntity = new BrokerEntity(id, name, confidence, stocks, price);
//                System.out.println(brokerEntity);
                brokerEntityList.add(brokerEntity);
            }
            return brokerEntityList;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void updateBroker(BrokerEntity brokerEntity) {
        try {
            Connection conn = DriverManager.getConnection(connectionUrl, "root", "root");
            String sqlUpdateBroker = "UPDATE broker SET confidence = ?, stocks = ?, price = ? WHERE (id = ?)";
            PreparedStatement ps = conn.prepareStatement(sqlUpdateBroker);
            ps.setInt(1, brokerEntity.getConfidence());
            ps.setInt(2, brokerEntity.getStocks());
            ps.setInt(3, brokerEntity.getPrice());
            ps.setInt(4, brokerEntity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}