package stocksTrading;

import database.BrokerEntity;
import database.MySQL;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class InvestorAgent {
    private final MySQL mySQL = new MySQL();
    private final int minInitialNet = 100;
    private final int maxInitialNet = 1000;
    // InvestorAgent characteristics
    private final int id;
    private final String name;
    private List<BrokerEntity> stocksAgents = new ArrayList<>();
    private int initialNet;
    private int net;
    private int confidenceToBuy;  //50 - min, 100 - max
    private int confidenceToSell; // 0 - min, 50 - max // when lower
    private Map<Integer, ValueStockBought> stocksBought = new HashMap<>();
    private WriteCSV writeCSV;

    protected InvestorAgent(int id, String name, String fileName) {
        this.id = id;
        this.name = name;
        initialNet = (int) (Math.random() * (maxInitialNet - minInitialNet + 1) + minInitialNet);
        net = initialNet;
        confidenceToBuy = (int) (Math.random() * (100 - 50 + 1) + 50);
        confidenceToSell = (int) (Math.random() * (50 - 0 + 1) + 0);

        writeCSV = new WriteCSV(fileName);

        System.out.println("New: " + this.toString());

        Thread trade = new Trade(this);
        trade.start();
    }

    protected void takeDown() {
        System.out.println("Terminating..." + this.toString());
    }

    public String toString() {
        return "Investor agent: " + name + "\n" +
                "id = " + id + "\n" +
                "money = " + initialNet + "\n" +
                "stocksBought = " + stocksBought + "\n" +
                "confidenceToBuy = " + confidenceToBuy + "\n" +
                "confidenceToSell = " + confidenceToSell + "\n" +
                "initialNet = " + initialNet + "\n" +
                "net = " + net + "\n" +
                "stocksValue = " + getStocksValue() + "\n" +
                "profit = " + getProfit() + "\n";
    }

    public int getStocksValue() {
        AtomicInteger stocksValue = new AtomicInteger();
        stocksBought.forEach((k, v) -> stocksValue.addAndGet(v.count * v.price));
        return stocksValue.intValue();
    }

    public int getProfit() {
        return (net + getStocksValue()) - initialNet;
    }

    public int getNet() {
        return net;
    }

    private void setNet(int net) {
        this.net = net;
    }

    public int getConfidenceToBuy() {
        return confidenceToBuy;
    }

    public void setConfidenceToBuy(int confidenceToBuy) {
        this.confidenceToBuy = confidenceToBuy;
    }

    public int getConfidenceToSell() {
        return confidenceToSell;
    }

    public void setConfidenceToSell(int confidenceToSell) {
        this.confidenceToSell = confidenceToSell;
    }

    private List<BrokerEntity> getStocksAgents() {
        return stocksAgents;
    }

    public void setStocksAgents(List<BrokerEntity> stocksAgents) {
        this.stocksAgents = stocksAgents;
    }

    private Map<Integer, ValueStockBought> getStocksBought() {
        return stocksBought;
    }

    private void setStocksBought(Map<Integer, ValueStockBought> stocksBought) {
        this.stocksBought = stocksBought;
    }

    private static class ValueStockBought {
        int price;
        int count;

        public ValueStockBought(int price, int count) {
            this.count = count;
            this.price = price;
        }

        @Override
        public String toString() {
            return "ValueStockBought{" +
                    "price=" + price +
                    ", count=" + count +
                    '}';
        }
    }

    private class Trade extends Thread {
        private InvestorAgent a;

        public Trade(InvestorAgent a) {
            this.a = a;
        }

        @Override
        public void run() {
            try {
                InetAddress ip = InetAddress.getByName("localhost");
                Socket s = new Socket(ip, 5056);
                DataInputStream dis = new DataInputStream(s.getInputStream());
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                while (true) {
                    updateConfidence();
                    a.setStocksAgents(mySQL.selectAllBrokers());
                    System.out.println("StocksAgents found:" + a.getStocksAgents());
                    buySellHold(dos);

                    sleep(30);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void buySellHold(DataOutputStream dos) throws IOException {
            for (BrokerEntity broker : a.getStocksAgents()) {
                int confidence = broker.getConfidence();
                int stocks = broker.getStocks();
                int price = broker.getPrice();
                int brokerId = broker.getId();

                // Yes! I want to BUY one stock
                if (broker.getConfidence() > a.getConfidenceToBuy() && stocks > 0 && a.getNet() > price) {
                    if (stocksBought.containsKey(brokerId)) {
                        stocksBought.put(brokerId, new ValueStockBought(price, stocksBought.get(brokerId).count + 1));
                    } else {
                        stocksBought.put(brokerId, new ValueStockBought(price, 1));
                    }

                    dos.writeUTF(brokerId + " BUY");
                    net = net - price;
                    System.out.println("InvestorAgent: " + a.name + " BUY: stocks: " + a.getStocksBought());
                }
                // Nope! I want to SELL one stock back to you if I had bought one
                else if (confidence < a.getConfidenceToSell() && stocksBought.containsKey(brokerId) && stocksBought.get(brokerId).count > 0) {
                    stocksBought.get(brokerId).count--;
                    stocksBought.get(brokerId).price = price;
                    net = net + price;

                    dos.writeUTF(brokerId + " SELL");
                    System.out.println("InvestorAgent: " + a.name + " SELL: stocks: " + a.getStocksBought());
                }
                // Hmm.. I will HOLD my stocks
                else if (stocksBought.containsKey(brokerId) && stocksBought.get(brokerId).count > 0) {
                    stocksBought.get(brokerId).price = price;

                    dos.writeUTF(brokerId + " HOLD");
                    System.out.println("InvestorAgent: " + a.name + " HOLD: stocks: " + a.getStocksBought());
                }
                a.setNet(net);
                a.setStocksBought(stocksBought);
            }
        }

        private void updateConfidence() throws IOException {
            int newConfidenceToBuy = a.getConfidenceToBuy() + (int) (Math.random() * (10 - (-10) + 1) + (-10));
            newConfidenceToBuy = Math.min(newConfidenceToBuy, 100);
            newConfidenceToBuy = Math.max(newConfidenceToBuy, 50);
            a.setConfidenceToBuy(newConfidenceToBuy);

            int newConfidenceToSell = a.getConfidenceToSell() + (int) (Math.random() * (10 - (-10) + 1) + (-10));
            newConfidenceToSell = Math.min(newConfidenceToSell, 50);
            newConfidenceToSell = Math.max(newConfidenceToSell, 0);
            a.setConfidenceToSell(newConfidenceToSell);
            System.out.println(a.toString());
            if (a.getNet() == 0 && a.getStocksValue() == 0) {
                a.takeDown();
            }

            writeCSV.writeData(new String[] {String.valueOf(a.id),
                    String.valueOf(ZonedDateTime.now().toInstant().toEpochMilli()),
                    String.valueOf(a.getProfit()),
                    String.valueOf(a.net),
                    String.valueOf(a.confidenceToSell),
                    String.valueOf(a.confidenceToBuy),
                    String.valueOf(a.initialNet),
            });
        }
    }
}