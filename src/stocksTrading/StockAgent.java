package stocksTrading;

import database.BrokerEntity;
import database.MySQL;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Time;
import java.time.LocalTime;
import java.time.ZonedDateTime;

public class StockAgent {
    private final MySQL mySQL = new MySQL();
    private final int minNumber = 1000;
    private final int maxNumber = 10000;
    private final int minPrice = 10;
    private final int maxPrice = 100;
    // StockAgent characteristic
    private final int id;
    private final String name;
    private int stocksTotal;
    private int stocksForSale;
    private int stocksForCompany;
    private int stockPrice;
    private int initialCapital;
    private int confidence; // 0 - zero conf, 100 - total confidence

    private WriteCSV writeCSV;

    protected StockAgent(int id, String name, String fileName) {
        this.id = id;
        this.name = name;
        stocksTotal = (int) (Math.random() * (maxNumber - minNumber + 1) + minNumber);
        stocksForSale = (int) (0.7 * stocksTotal);
        stocksForCompany = stocksTotal - stocksForSale;
        stockPrice = (int) (Math.random() * (maxPrice - minPrice + 1) + minPrice);
        initialCapital = stocksForCompany * stockPrice;
        confidence = (int) (Math.random() * (90 - 45 + 1) + 45);

        writeCSV = new WriteCSV(fileName);

        System.out.println("New: " + this.toString());

        Thread stockAgentTrade = new StockAgentTrade(this);
        stockAgentTrade.start();
    }

    protected void takeDown() {
        // todo: set in DB you're dead
        System.out.println("Terminating..." + this.toString());
    }

    public String toString() {
        return "\n" +
                "Company agent: " +
                "id = " + id + "\n" +
                "initialCapital = " + initialCapital + "\n" +
                "stocksNumber = " + stocksTotal + "\n" +
                "stocksForSale = " + stocksForSale + "\n" +
                "stocksForCompany = " + stocksForCompany + "\n" +
                "stockPrice = " + stockPrice + "\n" +
                "confidence = " + confidence + "\n" +
                "profit = " + getProfit() + "\n";
    }

    public int getProfit() {
        return stocksForCompany * stockPrice - initialCapital;
    }

    public int getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(int stockPrice) {
        this.stockPrice = stockPrice;
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }

    public int getStocksForSale() {
        return stocksForSale;
    }

    public void setStocksForSale(int stocksForSale) {
        this.stocksForSale = stocksForSale;
    }

    private class StockAgentTrade extends Thread {
        private StockAgent a;

        public StockAgentTrade(StockAgent a) {
            this.a = a;
        }

        @Override
        public void run() {
            try {
                InetAddress ip = InetAddress.getByName("localhost");
                Socket s = new Socket(ip, 5056);
                DataInputStream dis = new DataInputStream(s.getInputStream());
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                while (true) {
                    updateTrade(dos, dis);
                    updateConfidence();

                    sleep(30);
                }
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }

        private void updateTrade(DataOutputStream dos, DataInputStream dis) throws IOException {
            dos.writeUTF(a.id + " UPDATE");
            String received = dis.readUTF();

            String[] receivedList = received.split(" ");
            int count = Integer.parseInt(receivedList[0]);
            String action = receivedList[1];

            System.out.println("StockAgent-received: " + count + action);

            switch (action) {
                // Huraaah! He wants to BUY
                case "BUY":
                    a.setStocksForSale(a.getStocksForSale() - count);
                    a.setStockPrice((int) (a.getStockPrice() + a.getStockPrice() * 0.1));
                    break;
                // Oooh no! He wants to SELL
                case "SELL":
                    a.setStocksForSale(a.getStocksForSale() + count);
                    a.setStockPrice((int) (a.getStockPrice() - a.getStockPrice() * 0.08));
                    break;
                // Okay! He wants to HOLD
                case "HOLD":
                    a.setStockPrice((int) (a.getStockPrice() + a.getStockPrice() * 0.01));
                    break;
                case "BEAT":
                    break;
            }
        }

        private void updateConfidence() throws IOException {
            int newConfidence = a.getConfidence() + (int) (Math.random() * (5 - (-5) + 1) + (-5));
            newConfidence = Math.min(newConfidence, 100);
            newConfidence = Math.max(newConfidence, 0);
            a.setConfidence(newConfidence);

            mySQL.updateBroker(new BrokerEntity(a.id, a.name, a.confidence, a.stocksForSale, a.stockPrice));
            System.out.println(a.toString());
            // Debatable
            if (a.stocksForSale == 0) {
                a.takeDown();
            }

            writeCSV.writeData(new String[] {String.valueOf(a.id),
                    String.valueOf(ZonedDateTime.now().toInstant().toEpochMilli()),
                    String.valueOf(a.getProfit()),
                    String.valueOf(a.confidence),
                    String.valueOf(a.stockPrice),
                    String.valueOf(a.stocksForSale),
                    String.valueOf(a.initialCapital),
                    String.valueOf(a.stocksTotal),
                    String.valueOf(a.stocksForCompany),
            });
        }
    }
}
