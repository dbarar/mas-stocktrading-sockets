package stocksTrading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WriteCSV {
    public String fileName;

    public WriteCSV(String fileName) {
        this.fileName = "src/stocksTrading/filesSimulation/" + fileName;
    }

    public static void main(String[] args) throws IOException {
        String CSV_FILE_NAME = "src/stocksTrading/filesSimulation/file2.csv";
        List<String[]> dataLines = new ArrayList<>();
        dataLines.add(new String[]{"John", "Doe", "38", "Comment Data\nAnother line of comment data"});
        dataLines.add(new String[]{"Jane", "Doe, Jr.", "19", "She said \"I'm being quoted\""});
        stocksTrading.WriteCSV writeCSV1 = new stocksTrading.WriteCSV(CSV_FILE_NAME);
        writeCSV1.writeArrayData(dataLines, CSV_FILE_NAME);

        String CSV_FILE_NAME2 = "src/stocksTrading/filesSimulation/file3.csv";
        stocksTrading.WriteCSV writeCSV = new stocksTrading.WriteCSV(CSV_FILE_NAME2);
        writeCSV.writeData(new String[]{"John", "Doe", "38", "Comment Data\nAnother line of comment data"});
        writeCSV.writeData(new String[]{"John", "Doe", "39", "Comment Data\nAnother line of comment data"});
        writeCSV.writeData(new String[]{"John", "Doe", "38", "Comment Data\nAnother line of comment data"});
    }

    public String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    public void writeArrayData(List<String[]> dataLines, String fileName) throws IOException {
        File csvOutputFile = new File(fileName);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }
        System.out.println(csvOutputFile.exists());
    }

    public void writeData(String[] data) throws IOException {
        FileWriter csvOutputFile = new FileWriter(fileName, true);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            pw.println(this.convertToCSV(data));
        }
    }

}
